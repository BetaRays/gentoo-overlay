You can use a config file or `app-eselect/eselect-repository` to add this overlay.

`eselect-repository` command: `eselect repository add BetaRays git https://framagit.org/BetaRays/gentoo-overlay.git`

Example config file:
```
[BetaRays]
priority = 100
location = /var/lib/myoverlays/BetaRays
sync-type = git
sync-uri = https://framagit.org/BetaRays/gentoo-overlay.git
auto-sync = Yes
```
