# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

SRC_URI="https://github.com/fragglet/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="~amd64 ~x86"

DESCRIPTION="Free Software LHA implementation"
HOMEPAGE="http://fragglet.github.com/lhasa/"

LICENSE="ISC"
SLOT="0"

DOCS=( "README" "ChangeLog" "NEWS" "COPYING" "AUTHORS" "TODO" )

src_configure() {
	./autogen.sh
	default
}
