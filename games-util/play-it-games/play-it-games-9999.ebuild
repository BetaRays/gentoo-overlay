# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3
EGIT_REPO_URI='https://forge.dotslashplay.it/play.it/games.git'
SRC_URI=""
KEYWORDS=""

DESCRIPTION="Installer for drm-free commercial games, official game scripts"
HOMEPAGE="https://dotslashplay.it/"

LICENSE="BSD-2"
SLOT="0"

RDEPEND="games-util/play-it-lib"

src_install() {
	emake DESTDIR="${ED}" prefix="/usr" bindir="/usr/bin" datadir="/usr/share" install
}
