# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

SRC_URI="https://forge.dotslashplay.it/play.it/scripts/-/archive/${PV}/scripts-${PV}.tar.bz2 -> ${P}.tar.bz2"
KEYWORDS="~amd64 ~x86"
S="${WORKDIR}/scripts-${PV}"

DESCRIPTION="Installer for drm-free commercial games, core library"
HOMEPAGE="https://dotslashplay.it/"

LICENSE="BSD-2"
SLOT="0"
IUSE="debug"

src_compile() {
	emake DEBUG="$(usex debug 1 0)"
}

src_install() {
	emake DESTDIR="${ED}" prefix="/usr" bindir="/usr/bin" datadir="/usr/share" install
}

pkg_postinst() {
	einfo "You now need to install game scripts."
	einfo "games-util/play-it-games contains some."
}
