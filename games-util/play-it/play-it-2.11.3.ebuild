# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

if [[ ${PV} == 9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI='https://forge.dotslashplay.it/play.it/scripts.git'
	SRC_URI=""
	KEYWORDS=""
else
	SRC_URI="https://forge.dotslashplay.it/play.it/scripts/-/archive/${PV}/scripts-${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
	S="${WORKDIR}/scripts-${PV}"
fi

DESCRIPTION="Installer for drm-free commercial games"
HOMEPAGE="https://dotslashplay.it/"

LICENSE="BSD-2"
SLOT="0"
IUSE="doc"

CDEPEND="doc? ( app-text/pandoc )"

src_install() {
	emake DESTDIR="${ED}" prefix="/usr" bindir="/usr/bin" datadir="/usr/share" install
}
