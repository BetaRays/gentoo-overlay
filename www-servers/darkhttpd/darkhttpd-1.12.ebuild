# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

SRC_URI="https://unix4lyfe.org/${PN}/${P}.tar.bz2"
KEYWORDS="~amd64 ~arm64 ~x86"

DESCRIPTION="When you need a web server in a hurry."
HOMEPAGE="https://unix4lyfe.org/darkhttpd/"

LICENSE="BSD"
SLOT="0"

DOCS=( "README" "TODO" )

src_install() {
	dobin darkhttpd
}
